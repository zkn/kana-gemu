/**
 * kana.js
 *
 * Library for making a hiragana game
 */

//  Todo: Move the dictionary to a proper database and create an API to handle it.
const dictionary = {
  え: "e",
  は: "ha",
  こ: "ko",
  あ: "a",
  ぐ: "gu",
  ち: "chi",
  ね: "ne",
  み: "mi",
  れ: "re",
  う: "u",
  ざ: "za",
  ぺ: "pe",
  び: "bi",
  を: "wo",
  か: "ka",
  ぷ: "pu",
  ふ: "fu",
  ら: "ra",
  め: "me",
  へ: "he",
  ま: "ma",
  ぬ: "nu",
  ん: "n",
  ぎ: "gi",
  ぽ: "po",
  ひ: "hi",
  な: "na",
  わ: "wa",
  に: "ni",
  け: "ke",
  ぶ: "bu",
  ぜ: "ze",
  て: "te",
  ぱ: "pa",
  し: "shi",
  ば: "ba",
  げ: "ge",
  ご: "go",
  つ: "tsu",
  で: "de",
  じ: "ji",
  む: "mu",
  べ: "be",
  い: "i",
  さ: "sa",
  る: "ru",
  も: "mo",
  す: "su",
  が: "ga",
  お: "o",
  よ: "yo",
  き: "ki",
  ほ: "ho",
  だ: "da",
  の: "no",
  と: "to",
  ど: "do",
  や: "ya",
  ぞ: "zo",
  そ: "so",
  ぼ: "bo",
  せ: "se",
  ゆ: "yu",
  ず: "zu",
  く: "ku",
  り: "ri",
  ろ: "ro",
  ぴ: "pi",
  た: "ta"
};

function randomChoice(source) {
  let r = Math.floor(source.length * Math.random());
  return source[r];
}

function popFrom(source, v) {
  let r = [];
  for (let i = 0; i < source.length; i++) {
    let value = source[i];
    if (value !== v) {
      r.push(value);
    }
  }
  return r;
}

export { dictionary, randomChoice, popFrom };
