import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Deck from "./views/Deck.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/deck/:id",
      name: "deck",
      component: Deck
    }
  ]
});
